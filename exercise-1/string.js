let maRegexp = /[\'\*\+\/\-\[\]{}\$%\.,:;!\?\s\_]+/g;
let alphaRegExp = /[a-z]/gi;

function ucfirst(str){
    if(typeof str !== "string" || !str) return '';
    return str[0].toUpperCase() + str.substring(1);
}

function capitalize(str){
    if(typeof str !== "string" || !str) return '';
    return str.toLowerCase().split(maRegexp).map(function(item){return ucfirst(item);}).join(' ');
}

function camelCase(str) {
    if(typeof str !== "string" || !str) return '';
    return capitalize(str).replace(/(\s)/g, '');
}


function snake_case(str){
    if(typeof str !== "string" || !str) return '';
    return str.toLowerCase().replace(maRegexp, '_');
}

/* Non finit donc c'est la corection */
function leet(str) {
    if(typeof str !== "string" || !str) return '';
    return str.replace(/[aeiouy]/gi, function (item) {
        switch (item) {
            case 'a':
            case 'A':
                return 4;
            case 'e':
            case 'E':
                return 3;
            case 'i':
            case 'I':
                return 1;
            case 'o':
            case 'O':
                return 0;
            case 'u':
            case 'U':
                return '(_)';
            case 'y':
            case 'Y':
                return 7;
            default:
                return false;
        }
    });
}

function verlan(str) {
    if(typeof str !== "string" || !str) return '';
    return str.split(' ').map(function(word){
        return word.split('').reverse().join('');
    }).join(' ');
}

function yoda(str) {
    if(typeof str !== "string" || !str) return '';
    return str.split(' ').reverse().join(' ');
}

/* Fait par moi même */
function vig(str, code) {
    if(typeof str !== "string" || !str) return '';
/* Try myself
    let repet = (Math.round((str.replace(maRegexp, '').length/code.length)+.5));
    let codeBonneTaille;
    for(let i=1 ; i<repet ; i+=1) {
        codeBonneTaille += code;
    }
    let m = 1;
    str.replace(alphaRegExp, (e)=>{
        console.log(e);
        return 'x';
    });*/

    /* Correction */
    while(code.length < str.length) {
        code += code;
    }
    let codeIndex = 0;
    return str.split('').map(function(char, index){
        const charCode = char.charCodeAt(0) - "a".charCodeAt(0);
        if(charCode < 0 || charCode > 25) {
            return char;
        }
        const codeCode = code [codeIndex++].charCodeAt(0);
        const cryptedCode = (charCode + codeCode) % 26;
        return String.fromCharCode(cryptedCode); 
    }).join('');    
}

// 41 char ; 34 alpha ; 7 spec  ;                  7 char ; 7 alpha
// console.log('Chaine traitée : ',vig('j\'adore ecouter la radio toute la journee', 'musique'));


function prop_access1(obj, path) {
    try {
        if (obj === null) {
            console.log(path, "not exist")
            return;
        }
        if (path === null) {
            return obj
        }
        if (path === '') {
            return obj
        }
        let data = {...obj};
        let dir = [];
        if (!isStr(path)) {
            return obj;
        }
        for (value of path.split('.')) {
            if (typeof data[value] === 'undefined') {
            return dir.join('.');
            }
            dir.push(value);
            data = data[value];
        }
        return data;
    } catch (error) {
        console.log('prop_access error');
        console.log(error);
        return error;
    }
}

function prop_access(obj, path) {
    if (obj === null) {
        console.log('"', path, '" not exist')
        return;
    }
    let pathTab = path.split('.');
    const child = pathTab[0];
    pathTab.splice(0);
    path = pathTable.join('.');
    console.log(pathTab);
}

prop_access({}, 'boulou.erre.arrreerere.kjgsdf');

/* Tests *
let functions = [
    ucfirst,
    capitalize,
    camelCase,
    snake_case,
    leet,
    verlan,
    yoda
];

functions.forEach(element => {
    console.log('blabla : ', element('blabla'));
    console.log('bla bla : ', element('bla bla'));
    console.log('Bla bla BLA : ', element('Bla bla BLA'));
    console.log('bla_blaBla bla-bla : ', element('bla_blaBla bla-bla'));
    console.log('BLAbla blaBLA-blaB_LAbE : ', element('BLAbla blaBLA-blaB_LAbE'));
    console.log('Bili bouloblob_aha : ', element('Bili bouloblob_aha'));
    console.log('\'\' : ', element(''));
    console.log('object{} : ', element({}));
    console.log('FiNi_A prioris : ', element('FiNi_A prioris'));
    console.log('_____________________________________________________________');
});
/* */
