let tools = require('./string');

let functions = [
    tools.ucfirst
];

console.log('Tests for functions in ');


   // console.log(element);
console.log('blabla : ', tools.ucfirst('blabla'));
console.log('bla bla : ', tools.ucfirst('bla bla'));
console.log('Bla bla BLA : ', tools.ucfirst('Bla bla BLA'));
console.log('bla_blaBla bla-bla : ', tools.ucfirst('bla_blaBla bla-bla'));
console.log('BLAbla blaBLA-blaB_LAbE : ', tools.ucfirst('BLAbla blaBLA-blaB_LAbE'));
console.log(' : ', tools.ucfirst(''));
console.log('object{} : ', tools.ucfirst({}));
console.log('FiNi_A prioris : ', tools.ucfirst('FiNi_A prioris'));
console.log('_______________________________________________________');
/*
console.log('blabla : ', element('blabla'));
console.log('bla bla : ', element('bla bla'));
console.log('Bla bla BLA : ', element('Bla bla BLA'));
console.log('bla_blaBla bla-bla : ', element('bla_blaBla bla-bla'));
console.log('BLAbla blaBLA-blaB_LAbE : ', element('BLAbla blaBLA-blaB_LAbE'));
console.log(' : ', element(''));
console.log('object{} : ', {});
console.log('FiNi_A prioris : ', element('FiNi_A prioris'));
console.log('_______________________________________________________');
*/