// Type checker
function type_check_v1(toTest, typeToTest) {
    // On sort direct si le type du 2eme parametre n'est pas un string
    if(typeof typeToTest !== 'string') return false;
    if(typeof toTest !== "object")  {
        return typeof toTest === typeToTest;
    } else {
        if(typeToTest === 'null' && toTest === null) return true;
        if(typeToTest === 'array' && Array.isArray(toTest)) return true;
        if(typeToTest === 'object' && !Array.isArray(toTest) && toTest !== null) return true;
    }
    return false;
}

/* Tests type_check_v1 *
console.log('Object');
console.log(type_check_v1(null, 'object'));
console.log(type_check_v1({}, 'object'));
console.log(type_check_v1([], 'object'));
console.log(type_check_v1('tert', 'object'));
console.log(type_check_v1(54, 'object'));

console.log('string');
console.log(type_check_v1(null, 'string'));
console.log(type_check_v1({}, 'string'));
console.log(type_check_v1([], 'string'));
console.log(type_check_v1('tert', 'string'));
console.log(type_check_v1(54, 'string'));

console.log('number');
console.log(type_check_v1(null, 'number'));
console.log(type_check_v1({}, 'number'));
console.log(type_check_v1([], 'number'));
console.log(type_check_v1('tert', 'number'));
console.log(type_check_v1(54, 'number'));

console.log('array');
console.log(type_check_v1(null, 'array'));
console.log(type_check_v1({}, 'array'));
console.log(type_check_v1([], 'array'));
console.log(type_check_v1('tert', 'array'));
console.log(type_check_v1(54, 'array'));

console.log('Null');
console.log(type_check_v1(null, 'null'));
console.log(type_check_v1({}, 'null'));
console.log(type_check_v1([], 'null'));
console.log(type_check_v1('tert', 'null'));
console.log(type_check_v1(54, 'null'));
 
/* */

/**
 * 
 * @param {any} toTest L'item que l'on veut tester
 * @param {object} confToTest L'objet par lequel on teste
 */
function type_check_v2(toTest, confToTest) {
    // Si la config n'est pas un objet on renvoi a type_check_v1
    if (!type_check_v1(confToTest, 'object')) { 
        return type_check_v1(toTest, confToTest);
    }

    if (confToTest.hasOwnProperty('type')) {
        if (!type_check_v1(toTest, confToTest.type) || !type_check_v1(confToTest.type, 'string')) {
            // mauvais type
            return false;
        }
    }
    if (confToTest.hasOwnProperty('value')) {
        if (toTest !== confToTest.value) {
            // mauvaise value
            return false;
        }
    }
    if (confToTest.hasOwnProperty('enum')) {
        if(!type_check_v1(confToTest.enum, 'array')) return false;
        let i = 0, valid = false;
        while ((i < confToTest.enum.length) && !valid) {
            valid = (toTest === confToTest.enum[i]);
            i++;
        }
        return valid;
    }
    return true;
}

/* Tests type_check_v2 *
console.log('type_check_v2')
console.log(type_check_v2('something', {enum: ['something']}));
/* */


function type_check(toTest, confToTest) {
    // Si la config n'est pas un objet on renvoi a type_check_v1
    if (!type_check_v1(confToTest, 'object')) { 
        return type_check_v1(toTest, confToTest);
    }

    if (confToTest.hasOwnProperty('properties')) {
        if (type_check_v1(confToTest.properties, 'array')) {
            console.log('passed on array if')
            let i = 0, valid = true;
            while (i < confToTest.properties.length && valid) {
                console.log('passed on while loop')
                valid = intToBool(valid & type_check(toTest[i], confToTest.properties[i]));
                i++;
            }
            return valid;
        } else if (type_check_v1(confToTest.properties, 'object')) {
            let valid = true;
            Object.keys(confToTest.properties).forEach((elem) => {
                valid = intToBool(valid & type_check(toTest[elem], confToTest.properties[elem]));
            });
            return valid;
        } else {
            // a problem happened
            // si on parvient ici, properties n'est ni un objet ni un tableau, ce qui ne devrait pas arriver donc false pour ne rien laiser au hasard
            return false;
        }
    } else {
        return type_check_v2(toTest, confToTest);
    }
    return true;
}

/* Tests for type_check *   
const config = {
    type: 'object',
    properties: {
        prop1: { type: 'number' },
        prop2: { type: 'string', enum: ['val1', 'val2'] },
        prop3: { type: 'object', properties: { prop31: 'number' } },
        prop4: { type: 'array', properties: ['boolean'] }
    }
}
const toBeTestCorrect = {
    prop1: 7,
    prop2: 'val2',
    prop3: { prop31: 5 },
    prop4: [false]
}
const toBeTestNotCorrect = {
    prop1: '7',
    prop2: 'val2',
    prop3: { prop31: true },
    prop4: ['false']
}

console.log(type_check(toBeTestCorrect, config));
console.log(type_check(toBeTestNotCorrect, config));
/* */

/**
 * Any other number will return false
 * @param {number} i Should be 0 or 1
 */
function intToBool(i) { return i === 1; } // USEFULL

let me = {
    name: 'Etienne',
    age: '21',
    girlfriend: {
        name: 'Chaïna',
        age: 17
    },
    exGirlfriends: [
        {
            name: 'Emeline',
            age: 22
        },
        {
            name: 'Manon',
            age: 19
        }
    ]
}

let sameAsMe = {
    name: 'Etienne',
    age: '21',
    girlfriend: {
        name: 'Chaïna',
        age: 17
    },
    exGirlfriends: [
        {
            name: 'Emeline',
            age: 22
        },
        {
            name: 'Manon',
            age: 19
        }
    ]
}

let abdel = {
    name: 'Abdelhakim',
    age: '22',
    girlfriend: null,
    exGirlfriends: []
}

let nico = {
    name: 'Nicolas',
    age: '22',
    girlfriend: {
        name: 'Carla',
        age: 20
    },
    exGirlfriends: [
        {
            name: 'Jeane',
            age: 22
        }
    ]
}

const STRING = 'string', NUMBER='number', OBJECT='object', ARRAY='array', BOOLEAN='boolean', NULL='null';

/**
 * To verify if two objects had the same value.
 * 
 * @param {object} firstObj un des objet à comparer.
 * @param {object} secondObj le deuxième objet à comparer.
 * 
 * @return {boolean} true si les deux objets sont identiques, false sinon.
 */
function isTheSameObjects(firstItem, secondItem) {
    if (!type_check(firstItem, exactTypeOf(secondItem))) {
        return false;
    }
    let result = true
    if (type_check(firstItem, OBJECT)) {
        // Si c'est des objet
        const firstItemKeys = Object.keys(firstItem);
        if (firstItemKeys.length === Object.keys(secondItem).length) {
            // Si la taille est la même, c'est bon signe
            let compteur = 0;
            while ((compteur < firstItemKeys.length) && result) {
                if(secondItem.hasOwnProperty(firstItemKeys[compteur])) {
                    // Le second item a la même key que le premier
                    result = isTheSameObjects(firstItem[firstItemKeys[compteur]], secondItem[firstItemKeys[compteur]]);
                } else {
                    // Le second item n'a pas la key du premier item, et est donc différent
                    result = false;
                }
                compteur++;
            }
        } else {
            // Si la taille des objets est différente, ils ne sont pas identiques
            result = false;
        }
    } else if (type_check(firstItem, ARRAY)) {
        // Si c'est des tableau
        if (firstItem.length === secondItem.length) {
            // C'est bon signe
            let compteur = 0;
            while ((compteur < firstItem.length) && result) {
                result = isTheSameObjects(firstItem[compteur], secondItem[compteur]);
                compteur++;
            }
        } else {
            // Si pas la même taille, alors c'est différent
            result = false;
        }
    } else if (type_check(firstItem, NULL)) {
        // Si c'est un null
    } else {
        // Si c'est autre chose (string, number, boolean... )
        const criteria = {
            type: exactTypeOf(secondItem),
            value: secondItem
        };
        result = type_check (firstItem, criteria);
    }
    return result;
}

function exactTypeOf(o) {
    if(typeof o !== 'object')  {
        return typeof o;
    } else {
        if(o === null) return 'null';
        if(Array.isArray(o)) return 'array';
        if(!Array.isArray(o) && o !== null) return 'object';
    }
}


// console.log(isTheSameObjects(me, sameAsMe));

/**
 * @param {string} Yes
 */
Object.prototype.propAccess = prop_access;
/**
 * 
 * @param {string} someString 
 * @returns {any} 
 */
function prop_access(someString) {
    if (exactTypeOf(someString) === 'string') {
        if (someString.length > 0){
            const myArray = someString.split('.');
            if (myArray.length > 1) {
                propToAccess = myArray.splice(0, 1).join();
                if (this.hasOwnProperty(propToAccess)) {
                    someString = myArray.join('.');
                    return this[propToAccess].propAccess(someString);
                } else {
                    return console.error('The asked path don\'t exist.');
                }         
            } else {
                if (this.hasOwnProperty(someString)) {
                    return this[someString];
                } else {
                    return console.error('The asked path don\'t exist.');
                }
            }
        }
        return console.error('WTF');
    }
    return console.error('You should give me a string.');
}

// console.log(me.propAccess('exGirlfriends.2'));


String.prototype.interpolate = interpolate;

/**
 * 
 * @param {any} theObj 
 */
function interpolate(obj) {
    if (!type_check(obj, OBJECT)) {
        return console.error('A problem occured.');
    }
    let str = this;
    const regex = /{{ ?([a-zA-Z0-9.]*) ?}}/;
    //const interArray = str.match(regex);
    while ((interArray = regex.exec(str)) !== null) {
        // console.log(interArray[0]);
        // console.log(interArray[1]);
        // console.log(obj.propAccess(interArray[1]));
        str = str.replace(interArray[0], obj.propAccess(interArray[1]));
    }
    return str;
}

myString = 'Hey guy, I\'m {{ name }}, {{ age }} and {{girlfriend.name}} is my girlfriend. {{exGirlfriends.1.name}} & {{exGirlfriends.0.name}} are my ex girlfriends.';

console.log(myString.interpolate(me));


